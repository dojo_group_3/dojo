<?php

namespace App;

use PHPUnit\Framework\TestCase;

class MoveTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnStateBasedOnCommand()
    {
        $comand = new MoveCommand();

//        $move = new Move();
        $expectedState = new State();
        $nextState = $move->execute($comand);
        $this->assertEquals($expectedState, $nextState);
    }
}
