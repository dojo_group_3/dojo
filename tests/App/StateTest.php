<?php

namespace App;

use PHPUnit\Framework\TestCase;

class StateTest extends TestCase
{
    /**
     * @test
     */
    public function CanReturnNextStepByCommand()
    {
        $command = new MoveCommand();

        $expected_state = new State();

        $move           = new Move($command, $expected_state);
        $current_state  = new State([$move]);

        $returned_state = $expected_state->execute($command);

        $this->assertEquals($returned_state, $current_state);
    }
}
